
path=$(pwd)

spark-submit --class Main --master local[2] --driver-memory 8g \
--packages org.apache.spark:spark-sql-kafka-0-10_2.12:2.4.5 \
--jars $path/consume.jar,$path/jackson-module-scala_2.12-2.11.0.jar, \
$path/jackson-core-2.11.0.jar,$path/jackson-annotations-2.11.0.jar,$path/jackson-module-paranamer-2.11.0.jar, \
$path/jackson-databind-2.11.0.jar localhost:9092 topic