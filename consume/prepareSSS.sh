path=$(pwd)
resources_path='./resources/'
back_path='../'

rm *.jar
rm *.csv
rm *.zip

echo -e "Installing wget ..."

apt-get update
apt-get install -y wget
apt-get install unzip


echo -e "Getting all dependencies ..."

wget https://repo1.maven.org/maven2/com/fasterxml/jackson/module/jackson-module-scala_2.12/2.11.0/jackson-module-scala_2.12-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.11.0/jackson-databind-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.11.0/jackson-core-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.11.0/jackson-annotations-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/module/jackson-module-paranamer/2.11.0/jackson-module-paranamer-2.11.0.jar

wget https://gitlab.com/Plota/put-big2-proj2/-/raw/master/consume/consume.jar