path=$(pwd)
resources_path='./resources/'
back_path='../'

rm *.jar
rm *.csv
rm *.zip

echo -e "Installing wget ..."

apt-get update
apt-get install -y wget
apt-get install unzip

echo -e "Downloading data ..."
wget http://www.cs.put.poznan.pl/kjankiewicz/bigdata/projekt2/us-accidents.zip
unzip us-accidents.zip

mv $path/us-accidents $path/resources

cd $resources_path
rm weather.txt
rm geo*.csv
cd $back_path

echo -e "Getting all dependencies ..."

wget https://repo1.maven.org/maven2/com/fasterxml/jackson/module/jackson-module-scala_2.12/2.11.0/jackson-module-scala_2.12-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.11.0/jackson-databind-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.11.0/jackson-core-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.11.0/jackson-annotations-2.11.0.jar
wget https://repo1.maven.org/maven2/com/fasterxml/jackson/module/jackson-module-paranamer/2.11.0/jackson-module-paranamer-2.11.0.jar

wget https://gitlab.com/Plota/put-big2-proj2/-/raw/master/produce/produce_gcp.jar